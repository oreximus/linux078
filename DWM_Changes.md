## The customized changes of my DWM build!

- Using dwm-flexipatch as dwm main
	- vanitygaps enabled
	- dwmblocks enabled
- slstatus for statusbar
	- volume script is running for volume status in status bar.
- URxvt as default terminal-emulator
	- configured fonts as DejaVu Sans Mono for Powerline
	- for clipboard using urxvt-perl version below :
```
https://github.com/michaeltraxler/urxvt-perls
```
- using customized zsh-shell with oh-my-zsh and the theme is **rxvt-unicode-truecolor-wide-glyphs** which is available in aur repo of Arch.


## Some important links

[dwm build](https://github.com/oreximus/dwm)
[Xresources](https://github.com/oreximus/dotfiles/blob/main/dotfiles/.Xresources)
[zsh-theme](https://github.com/romkatv/powerlevel10k#installation)
[powerline fonts](https://github.com/powerline/fonts)
[picom.conf](https://github.com/oreximus/dotfiles/blob/main/configfiles/picom.conf)
[volumescript](https://github.com/oreximus/dotfiles/blob/main/scripts/volinfo)
