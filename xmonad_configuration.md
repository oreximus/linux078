## XMONAD Window manager Configuration for the First Time
## [👉Source👈](https://www.youtube.com/watch?v=3noK4GTmyMw)

- Install the **xmonad** from your repo or git.
- download the xmonad config file and copy it to your home dir.

### About Config File

- two dashes **--** are comments on the file.
- setting default terminal change the variable **myTerminal**.
- setting border width on the windows manager, **myBorderWidth**.
- changing modkey, **myModMask**.
- Restarting xmod with default modkey, press **Alt+Q**.
- Opening Terminal with Default Binding, **Alt+shift+enter**.
- Focused and Unfocused border colors, **myNormalBorderColor, myFocusedBorderColor**.
