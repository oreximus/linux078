## Flatpak configuration in any distro for the first time!

- Install the flatpak package from the repository
- Now add the flatpak-repo
```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```
