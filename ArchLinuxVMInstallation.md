# ArchLinux VM Installation in UEFI Mode on Virtualbox

This is a blog tutorial which shows how u can install the archlinux vm on your virtualbox hypervisor in UEFI mode.

### Resource Needed

- A Computer system having at least 4GB of RAM and a Dual Core Processor running.
- Virtualbox Hypervisor Installed on your machine
- ArchLinux ISO image

Just make sure that you've all of these requirement matched-up before getting started

### Creating an ArchLinux VM:

- Enter the Name of the Machine as u wish then Type as **Linux** and Version as **Arch Linux (64-bit).**

![img1.PNG](ArchLinuxVMInstallation/img1.png)

- Then Enter the Desired RAM size that you want to use but for now I'm using *4096* MB.

![img2.PNG](ArchLinuxVMInstallation/img2.png)

- Next, Select ***Create a virtual hard disk now*** Option and click on ***Create*** button.

![img3.PNG](ArchLinuxVMInstallation/img3.png)

- Provide the disk space according to you and leave the File location at default settings unless you have a reason to save it somewhere else and then click ***Create.***

![img6.PNG](ArchLinuxVMInstallation/img6.png)

- Click on ***Settings*** and Start the VM.

![img7.PNG](ArchLinuxVMInstallation/img7.png)

- Select the ***System*** option and on ***Motherboard*** Tab & check the '***Enable EFI (special OSes only)'*** option.
    
    ![img9.PNG](ArchLinuxVMInstallation/img9.png)
    

- On ***Processor*** tab enter the number of processors that u wanted to use I recommend you to use at least **2** processors to make your OS run perfectly or u can use more.

![img10.PNG](ArchLinuxVMInstallation/img10.png)

- Then u can Enable the **PAE/NX** and **Nested VT-x/AMD-V**, only if want these Features**.**

![img11.PNG](ArchLinuxVMInstallation/img11.png)

- On **Display** Option you can provide the ***Video memory*** and ***3D Acceleration*** to your VM.

![img12.PNG](ArchLinuxVMInstallation/img12.png)

- In **Storage** option select the ISO file of Arch and then Click on **OK.**

![img13.PNG](ArchLinuxVMInstallation/img13.png)

![img15.PNG](ArchLinuxVMInstallation/img15.png)

- Now finally start the VM and Begin the installation.

![img16.PNG](ArchLinuxVMInstallation/img16.png)

### Installation

- Configuring Time and Region:

```bash
timedatectl set-ntp true
```

```bash
timedatectl set-timezone Asia/Kolkata
```

- Creating partitions and formatting disks:

```bash
fdisk /dev/sda
```

- above ***/dev/sda*** is your disk name you can list-out the disks and verify their names by this command:

```bash
fdisk -l
```

![img1.PNG](ArchLinuxVMInstallation/img1%201.png)

- Partitioning inside **fdisk** utility:

![img2.PNG](ArchLinuxVMInstallation/img2%201.png)

First Partition will be our boot partition so select the partition type as ***'ef'*** which will associate it with **EFI Boot Partition** and then provide it a bootable flag by **a** option.

![img3.PNG](ArchLinuxVMInstallation/img3%201.png)

Second Partition will be our **swap partition** so select the partition type as ***'82'*** which will associate it with **Linux Swap Partition.**

![img4.PNG](ArchLinuxVMInstallation/img4.png)

Finally Create the third Partition which will be our **Linux** Partition, for that just create a **new primary partition** and hit enter for setting all default settings continuously and **p** to check the partitions then **w** for writing all disks.

![img5.PNG](ArchLinuxVMInstallation/img5.png)

- After creating partition now we need to format them:

Formatting first partition as **Fat 32:**

```bash
mkfs.fat -F 32 /dev/sda1
```

Formatting second partition as **swap:**

```bash
mkswap /dev/sda2
```

Formatting third partition as **ext4** filesystem:

```bash
mkfs.ext4 /dev/sda3
```

- Mounting filesystem and installing the base system:

mounting ext4 filesystem to /mnt directory:

```bash
mount /dev/sda3 /mnt
```

- Installing the base system:

```bash
pacstrap /mnt base base-devel linux linux-firmware vim
```

- After finishing the installation, now attach the system by:

```bash
arch-chroot /mnt
```

- Configuring the **time-date** and **region** again for the system:

```bash
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
```

```bash
hwclock --systohc
```

- Configuring Language:

```bash
vim /etc/locale.gen
```

- Find out **en_US.UTF-8** and uncomment it, if you want to use english as your default language or u can use other languages.
- then save this text:

```bash
echo "LANG=en_US.UTF-8" > /etc/locale.conf
```

- and run this command:

```bash
locale-gen
```

- Now we need to set hostname and edit hosts file:

```bash
echo "hostname" > /etc/hostname
```

```bash
vim /etc/hosts
```

- After executing above command you need add these line in opened **hosts** file:

```bash
127.0.0.1		localhost
::1			localhost
127.0.1.1		hostname
```

- And then save it and close the file.

Now set the root password and create new user:

```bash
passwd
```

Enter the Root Password Twice!

```bash
useradd -g users -G storage,wheel,power -m username
```

- After creating user with some useful groups you need to configure a password for it:

```bash
passwd username
```

Enter the User Password twice!

- Now provide the sudo privileges to user for that u need edit this file:

```bash
EDITOR=vim visudo
```

- Find **%wheel** string and uncomment the whole line and save&exit from that file.
- And then install these packages:

```bash
pacman -S grub efibootmgr networkmanager dhcpcd
```

- Then install grub:

first make **EFI** named directory inside **/boot/** directory and mount the first partition (our bootable partition) to it.

```bash
mkdir /boot/EFI
```

```bash
mount /dev/sda1 /boot/EFI
```

then finally install the grub bootloader:

```bash
grub-install --target=x86_64-efi --efi-directory=/boot/EFI
```

now make **grub.cfg** file

```bash
grub-mkconfig -o /boot/grub/grub.cfg
```

- If everything done perfectly, then enable the following processes:

```bash
systemctl enable NetworkManager && systemctl enable dhcpcd
```

- Now exit from the attached system by:

```bash
exit
```

- Now Reboot your computer.

```bash
reboot
```

And if you get to boot successfully then congrats! you've installed arch linux.
