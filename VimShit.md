## Below there is the collection of some vim commands that are usually very beneficial while using VIM:

- for replacing or deleting some certain text:
```
:%s/.*text.*/n\\gc
```
